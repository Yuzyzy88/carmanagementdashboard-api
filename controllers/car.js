const { Car } = require("../models");

module.exports = {
    create: async (req, res) => {
        try {
            const body = req.body;
            const data = await Car.create(body);
            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: " Data successfully created"
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error
            });
        }
    },

    list: async (req, res) => {
        try {
            const data = await Car.findAll();

            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: " Data successfully populated"
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error
            });
        }
    },

    destroy: async (req, res) => {
        try {
            const data = await Car.destroy({where:{id:req.params.id}});

            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: " Data successfully deleted"
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error
            });
        }
    },

    update: async (req, res) => {
        try {
            //console.log("masuk");
            const body = req.body;
            //console.log("masuk:", body);
            const data = await Car.update(body,{where:{id:req.params.id}});
            console.log("masuk:", data);

            return res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: " Data successfully update"
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error
            });
        }
    },
};
## Description 
Implemented rest API using Sequelize linked to postgresql to create, update, delete for project car management

## Tech
- [Express.js v4.17.3.](http://expressjs.com/)
- [Sequelize v6.19.0](https://sequelize.org/docs/v6/getting-started/)
- [Postgresql](https://www.postgresql.org/)

## How to use 
- Install postgresql
- [Install postman](https://www.postman.com/downloads/?utm_source=postman-home)
- Clone this code
- Change the data according to your postgresql account in config/config.json
- Install "npm install"
- Run command "nodemon index.js"


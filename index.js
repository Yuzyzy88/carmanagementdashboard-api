const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const {create, list, destroy, update} = require('./controllers/car');

// pasang json parse middleware
app.use(express.json());

// router
app.post("/cars", create); // create
app.get("/cars/",list); // get all
app.delete("/cars/:id", destroy); // delete
app.put("/cars/:id", update); //update

app.listen(port, () => console.log(`Listening on http://localhost:${port}`));